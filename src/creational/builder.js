
class Boat {
  constructor({ ID, length, beam, buildingTime, color }) {
    this.ID = ID;
    this.length =length;
    this.beam = beam;
    this.buildingTime = buildingTime || 0;
    this.color = color;
  }

  // ...
}

class BoatBuilder {
  constructor(ID) {
    this.ID = ID;
  }

  defineLength(length) {
    this.length = length;
    return this;
  }

  defineBeam(beam) {
    this.beam = beam;
    return this;
  }

  setBuildingTime() {
    this.buildingTime = 100;
    return this;
  }

  paint(color) {
    this.color = color;
    return this
  }

  build() {
    return new Boat(this);
  }
}

/*
  const Boat = new BoatBuilder('Titanic')
    .defineLength(200)
    .defineBeam(70)
    .setBuildingTime()
    .paint('blue')
    .build();
*/

export default BoatBuilder;
