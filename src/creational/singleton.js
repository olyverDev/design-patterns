// Just object literal is singleton
export const OldSchoolLife = {
  live() { console.log('okay, living..'); },
  die() {
    Object.keys(this).forEach(ability => {
      delete this[ability];
    });
  },
  liveForever(inHeaven) {
    if (inHeaven) {
      delete this.die;
      delete this.liveForever;
    } else {
      throw new Error();
    }
   },
};

// life is single
class LifeClass {
  static cache = null;

  constructor() {
    if (!LifeClass.cache) {
      LifeClass.cache = this;

      return LifeClass.cache;
    }

    return LifeClass.cache;
  }

  // fill your (class-like) life on your own
}

/*
  const life = new LifeClass();
  const otherLife = new LifeClass();
  life === otherLife // true
*/


// life is one
const LifeSingletonFactory = () => {
  let instance;
  const createInstance = () => ({ news: "It's my life!" });

  return {
      create() {
          if (!instance) {
              instance = createInstance();
          }

          return instance;
      }
  };
};

/*
  const lifeCreator = LifeSingletonFactory();
  const life = lifeCreator.create();
  const otherLife = lifeCreator.create();
  life === otherLife // true
*/

export { LifeClass, LifeSingletonFactory, OldSchoolLife };
