
const WhiskeyFactory = ({ ID, name, country }) => ({
  ID,
  name,
  country,
  bottleVolume: 500,
  drink(howMuch) {
    this.bottleVolume -= Math.min(this.bootleVolume, howMuch);
  },
});

export default WhiskeyFactory;
