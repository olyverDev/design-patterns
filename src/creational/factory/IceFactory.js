// LOL thanks to https://medium.com/free-code-camp/elegant-patterns-in-modern-javascript-ice-factory-4161859a0eee
// some ice for whiskey from ./WhiskeyFactory

// in case immutability is needed
const IceFactory = ({ waterType, hardness }) => Object.freeze({
  waterType,
  hardness,
});

export default IceFactory;