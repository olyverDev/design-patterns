/**
 * @description DISCLAIMER: 18+ metaphor
 * https://www.geeksforgeeks.org/adapter-pattern/
 */

class Сourtesan {
  serve(customer) {}
  blow(dick) {}
}

class EliteСourtesan {
  readPoems(customerEars) {}
  blowDeep(dick, balls = []) {}
}

class EliteСourtesanAdapter {
  constructor(eliteСourtesan) {
    this.courtesan = eliteСourtesan;
  }

  blow(dick) {
    this.courtesan.blowDeep(dick);
  }

  serve(customer) {
    this.courtesan.readPoems(customer.ears); // :(
  }
}

class Customer {
  use(courtesan) {
    try {
      courtesan.blow(this.dick);
      courtesan.serve(this);
    } catch (error) {
      throw new Error('Error: cannot blow and / or serve');
    }
  }
}

// const ordinaryСourtesan = new Сourtesan();
// const eliteСourtesan = new EliteСourtesan();
// const adaptedEliteСourtesan = new EliteСourtesanAdapter(eliteСourtesan);

// const bordelloCustomer = new Customer();
// bordelloCustomer.use(ordinaryСourtesan); // happy
// bordelloCustomer.use(eliteСourtesan); // error, there are no 'blow' and / or 'serve' abilities
// bordelloCustomer.use(adaptedEliteСourtesan); // happy
