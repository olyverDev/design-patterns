
// TODO: think about metaphor (not hackneyed like newspapers etc)

class Observer {
  constructor(ID) {
    this.ID = ID;
  }

  notify(data, processor = () => console.log(this.ID, ' is notified about ', data)) {
    return processor(data);
  }
}

class Subject {
  observers = [];

  setState(state) {
    this.state = state;

    if (!this.observers.length) {
      console.log('Nobody to notify');
    } else {
      this._notifyObservers(state);
    }
  }

  registerObserver(observer) {
    this.observers.push(observer);
  }

  unregisterObserver(observer) {
    this.observers = this.observers.filter(obs => obs !== observer);
  }

  _notifyObservers(state) {
    this.observers.forEach(observer => observer.notify(state));
  }
}

const observable = new Subject();
const observer1 = new Observer('observer1');
const observer2 =  new Observer('observer2');

observable.registerObserver(observer1);
observable.registerObserver(observer2);

observable.setState(20); // notified

observable.unregisterObserver(observer1);
observable.unregisterObserver(observer2);

observable.setState(20); // nothing happens
