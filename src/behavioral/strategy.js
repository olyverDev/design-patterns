
// aka policy pattern

const RTL_LANGUAGES = [
  'Arabic'
  'Aramaic'
  'Azeri'
  'Dhivehi'
  'Hebrew'
  'Kurdish'
  'Persian'
  'Urdu'
];

const STRATEGIES: {
  ltr: () => { /* ...logic */ console.log('ltr'); },
  rtl: () => { /* ...logic */ console.log('rtl'); },
};

const isRTLLanguage = (language) =>
  RTL_LANGUAGES.includes(language);

// context
const displayText = (text, language) => {
  const strategy = isRTLLanguage(language) ? STRATEGIES.rtl : STRATEGIES.ltr;

  return strategy(text);
}

export default displayText;
