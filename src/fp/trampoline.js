// "Jump on the trampoline" mepathor for recurvsive functions optimization

const TIREDNESS_LIMIT = 10000; // to exceed call stack

// if we have tail call optimization it's okay
const jump = (tiredness = 0) => {
  console.log('jump!', `tiredeness is ${tiredness}`);

  if (tiredness >= TIREDNESS_LIMIT) {
    console.log('tired!');
    return;
  }

  return jump(tiredness + 1);
};

// we're getting rid of bloating the stack by such flow
const trampoline = (fn) => (...args) => {
  let result = fn(...args);

  while (typeof result === 'function') {
    result = result();
  }

  return result;
};

const jumpOptimized = (tiredness = 0) => {
  console.log('jump!',  `tiredeness is ${tiredness}`);

  if (tiredness >= TIREDNESS_LIMIT) {
    console.log('tired!');
    return;
  }

  return () => jumpOptimized(tiredness + 1);
};

const jumpWithTrampoline = trampoline(jumpOptimized);

// jump(); // stack overflow
jumpWithTrampoline(); // works
