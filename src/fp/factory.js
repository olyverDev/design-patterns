// currying as function factory
// also lazy evaluation could fit there

const addFactory = num => other => num + other;

const increment = add(1);
const decrement = add(-1);
const addBakersDozen = addFactory(13);
// increment(1) // -> 2
// decrement (1) // -> 0
// add10(5) // -> 15

export { addFactory, increment, decrement, addBakersDozen };
