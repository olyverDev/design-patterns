
# Trying to implement popular (and not only) design patterns using software (and not only) metaphors

# Resources:
* "Code complete" 2nd edition by Steve McConnell
* https://github.com/sohamkamani/javascript-design-patterns-for-humans
* https://www.joezimjs.com/
* https://www.geeksforgeeks.org patterns
* https://medium.com/free-code-camp/elegant-patterns-in-modern-javascript-ice-factory-4161859a0eee
* https://www.dofactory.com/
* https://github.com/getify/Functional-Light-JS book
* https://blog.logrocket.com/using-trampolines-to-manage-large-recursive-loops-in-javascript-d8c9db095ae3/
